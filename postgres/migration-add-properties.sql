/*
 * migration-add-properties.sql
 * Copyright (C) 2019 rgregoir <rgregoir@laurier>
 *
 * Distributed under terms of the MIT license.
 */

ALTER TABLE samples
ADD COLUMN pipeline varchar(100) null,
ADD COLUMN "status" varchar(100) null,
ADD COLUMN project  varchar(100) null
;


-- vim:et
