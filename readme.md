
# genpipes-dashboard

The genpipes-dashboard is a web portal the shows the status of various jobs run by
genpipes.

## Install

Install the following tools:

  - `docker`
  - `node`/`npm`
  - `GNU make`

Then, from the root of the project:

 - `make build--dev` (fast development mode build, uses filesystem files as a shared volume)
 - `make run--dev` to start the application & database
 - Next, restore the database with:
   - `docker exec -it analysis-db psql -U postgres -c 'DROP TABLE IF EXISTS samples'`
   - `docker cp ./genpipes-dashboard-dump.sql analysis-db:/tmp/dump.sql`
   - `docker exec -it analysis-db psql -U postgres -f /tmp/dump.sql`

The application runs on port 3001 by default.

## Managing

The following commands are available to manage the application.

| Command             | Description                                                                    |
| --                  | --                                                                             |
| `make run`          | Starts the database & app                                                      |
| `make run--dev`     | Starts the database & app in dev mode (reloads the app when files are changed) |
| `make run/db`       | Starts the database                                                            |
| `make run/app`      | Starts the application                                                         |
| `make run/app--dev` | Starts the application (dev mode)                                              |
| `make build`        | Rebuilds the docker containers (**default target**)                            |
| `make build--dev`   | Rebuilds the docker containers (dev mode)                                      |
| `make stop`         | Stops the docker containers                                                    |
| `make clean`        | Cleans database data (required when SQL schema changes)                        |

## Structure

The application is structured as such:

 - `/postgres` : database docker container description and schemas
 - `/src` : nodejs app docker container and app source
 - `/src/client` : font-end part of the interface
 - `/data` : contains database data (*generated on `make build`*). Note that
     this folder is mounted as a volume in a docker container, thus docker
     changes the owner to 9999. Permission issues may arise.

## Architecture

This application is built around a single object, the `sample`. The data is submitted to this application
by the [watch_portal_folder.py](https://bitbucket.org/mugqic/genpipes/src/master/utils/watch_portal_folder.py)
script of the genpipes repsository. This script is called by a cron that runs on the app server, SSHes into
the different clusters (beluga, cedar, graham) with a CC custom user, and calls the script on those machines
so they send back their data to the app (see [./src/call-remote-scripts.js](./src/call-remote-scripts.js)
for details). The data is sent back as JSON, either the full blob if it's the first time transferring that object,
or a diff of the latest changes if the object was already seen.

The frontend only job is to present the samples in a pretty interface, the data is not transformed in any way.

## Backups

Backup the application by making a copy of the `/data` folder.

An example script is provided in `pipes-dashboard-backup`, which can be placed
in the `/etc/cron.daily` folder of your machine. Make sure to have the `zip`
utility installed and to adjust the paths.
