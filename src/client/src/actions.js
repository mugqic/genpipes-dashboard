/*
 * actions.js
 */

import { set, lensPath } from 'ramda'
import * as Requests from './requests'
import { normalizeSamples, averageStepTime } from './models'

export const SET_PARAM = 'SET_PARAM'
export const CLEAR_PARAMS = 'CLEAR_PARAMS'
export const SET_ORDER = 'SET_ORDER'
export const SET_ORDER_DIRECTION = 'SET_ORDER_DIRECTION'
export const INITIALIZE_SEARCH_PARAMS = 'INITIALIZE_SEARCH_PARAMS'
export const SET_ERROR_MESSAGE = 'SET_ERROR_MESSAGE'
export const CLEAR_ERROR_MESSAGE = 'CLEAR_ERROR_MESSAGE'

export const REQUEST_SAMPLES       = 'REQUEST_SAMPLES'
export const RECEIVE_SAMPLES       = 'RECEIVE_SAMPLES'
export const REQUEST_UPDATE_SAMPLE = 'REQUEST_UPDATE_SAMPLE'
export const RECEIVE_UPDATE_SAMPLE = 'RECEIVE_UPDATE_SAMPLE'

export const SET_PROJECT_CALCULATIONS = 'SET_PROJECT_CALCULATIONS'

export function setParam(which, value) {
  return {
    type: SET_PARAM,
    which,
    value
  }
}

export function initializeParams(params){
  return {
    type: INITIALIZE_SEARCH_PARAMS,
    payload: params,
  }
}

export function clearParams() {
  return {
    type: CLEAR_PARAMS
  }
}

export function setOrder(property) {
  return {
    type: SET_ORDER,
    property
  }
}

export function setOrderDirection(ascending) {
  return {
    type: SET_ORDER_DIRECTION,
    ascending
  }
}

export function setModifiedRange(after, before) {
 return (dispatch, getState) => {
    dispatch(setParam('modifiedAfter',  after))
    dispatch(setParam('modifiedBefore', before))
 }
}

export function requestSamples() {
  return {
    type: REQUEST_SAMPLES
  }
}

export function receiveSamples(samples) {
  return {
    type: RECEIVE_SAMPLES,
    samples
  }
}

export function setParamAndSearch(which, value) {
  return (dispatch, getState) => {
    dispatch(setParam(which,value))
    dispatch(fetchSamples())
  }
}

export function clearParamsAndSearch() {
  return (dispatch, getState) => {
    dispatch(clearParams())
    dispatch(fetchSamples())
  }
}

export function fetchSamples() {
  return (dispatch, getState) => {
    const { ui: { params, errorMessage }, samples } = getState()
    if (errorMessage)
      dispatch(clearErrorMessage())
    if (samples.isLoading)
      return

    dispatch(requestSamples())

    Requests.samples.search(params) // api call to get samples based on params
    .then(normalizeSamples)
    .then(samples => dispatch(receiveSamples(samples)))

    .catch(err => {
      dispatch(receiveSamples([]))
      console.error(err)
      dispatch(setErrorMessage('Error occured while fetching samples: ' + err.message))
    })
  }
}

export function requestUpdateSample(id, data) {
  return {
    type: REQUEST_UPDATE_SAMPLE,
    id,
    data
  }
}

export function receiveUpdateSample(id, data) {
  return {
    type: RECEIVE_UPDATE_SAMPLE,
    id,
    data
  }
}

export function updateSample(id, data) {
  return (dispatch, getState) => {
    const { samples } = getState()

    if (samples.isLoading)
      return

    dispatch(requestUpdateSample(id, data))

    Requests.samples.update(id, data)
    .then(res => dispatch(receiveUpdateSample(id, data)))
    .catch(err => {
      // Restore previous data
      dispatch(receiveUpdateSample(id, samples.list.find(sample => sample.id === id)))
      console.error(err)
      // TODO handle failure
      dispatch(setErrorMessage('Error occured while updating sample: ' + err.message))
    })
  }
}

export function setErrorMessage(message){
  return {
    type: SET_ERROR_MESSAGE,
    payload: message
  }
}

export function clearErrorMessage(){
  return {
    type:  CLEAR_ERROR_MESSAGE
  }
}

export function setProjectCalculations(sample) {
  return (dispatch, getState) => {
    const { samples, projects } = getState()

    const projectName = sample.data.project

    const projectCalculations = projects[projectName]

    if (projectCalculations)
      return

    const steps = sample.data.pipeline.step

    const calculations = []

    for (let i = 0; i < steps.length; i++) {
      const averageTime = averageStepTime(
          projectName,
          steps[i].name,
          samples.list)

      calculations.push(averageTime)
    }

    dispatch({
      type: SET_PROJECT_CALCULATIONS,
      payload: { name: projectName, calculations },
    })
  }
}
