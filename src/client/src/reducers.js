import { whereEq } from 'ramda';
import { set, find } from 'partial.lenses/dist/partial.lenses.cjs.js';
import { subDays } from 'date-fns'

import {
    REQUEST_SAMPLES
  , RECEIVE_SAMPLES
  , REQUEST_UPDATE_SAMPLE
  , RECEIVE_UPDATE_SAMPLE
  , SET_PARAM
  , CLEAR_PARAMS
  , SET_ORDER
  , SET_ORDER_DIRECTION
  , INITIALIZE_SEARCH_PARAMS
  , SET_ERROR_MESSAGE
  , CLEAR_ERROR_MESSAGE
  , SET_PROJECT_CALCULATIONS
} from './actions';
import { PROPERTIES } from './constants';


const uiInititalState = {
  errorMessage: undefined,
  ordering: {
    property: Object.keys(PROPERTIES)[0],
    ascending: true
  },
  params: {
    modifiedAfter: subDays(new Date(), 1),
    modifiedBefore: new Date(),
    pipeline: undefined,
    user: undefined,
    status: undefined,
    project: undefined,
  },
  searchParams: {
    paramsLoaded: false,
    users: [],
    pipelines: [],
    projects: [],
    statuss: [],
  }
}

function uiReducer(state = uiInititalState, action, data) {
  switch (action.type) {
    case SET_PARAM: {
      return { ...state, params: { ...state.params, [action.which]: action.value } }
    }
    case CLEAR_PARAMS: {
      return { ...state, params: uiInititalState.params }
    }
    case SET_ORDER: {
      return { ...state, ordering: { ...state.ordering, property: action.property } }
    }
    case SET_ORDER_DIRECTION: {
      return { ...state, ordering: { ...state.ordering, ascending: action.ascending } }
    }
    case INITIALIZE_SEARCH_PARAMS: {
      return { ...state, searchParams: action.payload, paramsLoaded: true }
    }
    case SET_ERROR_MESSAGE: {
      return { ...state, errorMessage: action.payload }
    }
    case CLEAR_ERROR_MESSAGE: {
      return { ...state, errorMessage: undefined }
    }
    default:
      return state;
  }
}


const samplesInititalState = {
  isLoading: false,
  list: [],
  projectsByName: [],
}

function samplesReducer(state = samplesInititalState, action, ui) {
  switch (action.type) {
    case REQUEST_SAMPLES: {
      return { ...state, isLoading: true }
    }
    case RECEIVE_SAMPLES: {
      return { ...state, isLoading: false, list: action.samples }
    }
    case REQUEST_UPDATE_SAMPLE: {
      return { ...state, list: set([find(whereEq({ id: action.id })), 'data'], action.data, state.list) }
    }
    case RECEIVE_UPDATE_SAMPLE: {
      return { ...state, list: set([find(whereEq({ id: action.id })), 'data'], action.data, state.list) }
    }
    default:
      return state;
  }
}


const projectsInitialState = {}

function projectsReducer(state = projectsInitialState, action) {
  switch (action.type) {
    case RECEIVE_SAMPLES: {
      return {}
    }
    case SET_PROJECT_CALCULATIONS: {
      return { ...state, [action.payload.name]: action.payload.calculations }
    }
    default:
      return state;
  }
}

export const rootReducer = (state = {}, action) => {
  const samples = samplesReducer(state.samples, action)
  const projects = projectsReducer(state.projects, action)
  const ui = uiReducer(state.ui, action)

  return { ui, samples, projects }
}
