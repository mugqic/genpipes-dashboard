/*
 * models.js
 */

const { SAMPLE_STATUS, STEP_STATUS, PROPERTIES } = require('./constants');


function normalizeSamples(samples) {
  samples.forEach(normalizeSample)

  return samples
}

function normalizeSample(sample) {
  sample.created  = new Date(sample.created)
  sample.modified = new Date(sample.modified)

  const folder = sample.data.pipeline.general_information.analysis_folder
  const parts = folder.split('/')
  const index = parts.findIndex(i => i === 'projects')
  const project = index ? parts.slice(index + 1, index + 3).join('/') : undefined
  sample.project = project

  return sample
}

/**
 * Returns status of a sample, which is the status of the last non-undetermined step
 * @param {Sample} sample
 * @returns {SampleStatus}
 */
function getSampleStatus(sample) {
  const steps = sample.data.pipeline.step

  for (let i = steps.length - 1; i >= 0; i--) {
    const step = steps[i]
    const status = getStepStatus(step)
    if (status !== STEP_STATUS.UNDETERMINED && status !== STEP_STATUS.QUEUED)
      return status
  }

  return SAMPLE_STATUS.UNDETERMINED
}

/**
 * Returns status of a step
 * @param {Step} step
 * @returns {StepStatus}
 */
function getStepStatus(step) {
  if (step.status === 'success')
    return STEP_STATUS.SUCCESS

  if (step.status === 'error')
    return STEP_STATUS.ERROR

  // Then, if step.status is undefined, we compute the status from jobs statuses

  if (step.job.every(job => job.status === 'success'))
    return STEP_STATUS.SUCCESS

  if (step.job.some(job => job.status === 'error'))
    return STEP_STATUS.ERROR

  if (step.job.some(isRunning))
    return STEP_STATUS.RUNNING

  if (step.job.some(job => job.status === undefined))
    return STEP_STATUS.QUEUED

  return STEP_STATUS.UNDETERMINED
}

function isRunning(job) {
  return ('job_start_date' in job) && !('job_end_date' in job)
}

/**
 * Returns time in ms between two dates
 * @param {Date} start
 * @param {Date} end
 * @returns {Number}
 */
function timeTaken(start, end) {
  return Math.abs(new Date(end).getTime() - new Date(start).getTime())
}

/**
 * Returns average time of one step throughout a project
 * @param {string} project
 * @param {string} stepName
 * @param {Object[]} samples
 * @returns {Number}
 */
function averageStepTime(project, stepName, samples) {
  const filteredSamples = samples.filter(u => u.data.project === project)

  let totalStepTime = 0
  let numberOfSteps = 0

  filteredSamples.forEach(sample => {
    const steps = sample.data.pipeline.step.filter(u => u.name === stepName)
    steps.forEach(step => {
      if (getStepStatus(step) !== STEP_STATUS.QUEUED) {
        step.job.forEach(job => {
          if (job.job_start_date && job.job_end_date)
            totalStepTime += timeTaken(job.job_start_date, job.job_end_date)
        })
        numberOfSteps++
      }
    })
  })
  return totalStepTime / numberOfSteps
}

module.exports = {
  getSampleStatus,
  getStepStatus,
  isRunning,
  normalizeSample,
  normalizeSamples,
  averageStepTime,
  timeTaken,
}
