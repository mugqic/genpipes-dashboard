/*
 * Samples.js
 */

import React, { Component } from 'react';
import prop from 'prop-types'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Popover,
  PopoverHeader,
  PopoverBody,
} from 'reactstrap';
import { set, lensPath } from 'ramda'
import msToHms from 'ms-to-hms'
import List from 'react-virtualized/dist/commonjs/List'
import AutoSizer from 'react-virtualized-auto-sizer'

import { updateSample, setProjectCalculations } from '../actions'
import humanReadableTime from '../helpers/humanReadableTime.js';
import Step from './Step'
import StepStatusBar from './StepStatusBar';
import model from '../models'
import { STEP_STATUS } from '../constants'

const EMPTY_TIME = '0:00:00'

const Span = ({ children }) => (
  <span title={children}>
    { children }
  </span>
)


const mapStateToProps = state => ({
  isLoading: state.samples.isLoading,
  projects: state.projects,
})
const mapDispatchToProps = dispatch =>
  bindActionCreators({ updateSample, setProjectCalculations }, dispatch)

class Samples extends Component {
  constructor() {
    super()

    this.togglePopover = this.togglePopover.bind(this)
    this.openPopover = this.openPopover.bind(this)
    this.onUpdateStatus = this.onUpdateStatus.bind(this)
    this.rowRenderer = this.rowRenderer.bind(this);

    this.state = {
      popoverOpen: false,
      popoverTarget: document.body,
      popoverData: undefined,
      projectPopupID: undefined,
    }
  }

  onUpdateStatus = (sample, step, index, status) => {
    const data = set(lensPath(['pipeline', 'step', index, 'status']), status, sample.data)
    this.props.updateSample(sample.id, data)
  }

  openProjectPopup = (projectPopupID) => {
    this.setState({ projectPopupID })
  }

  closeProjectPopup = () => {
    this.setState({ projectPopupID: undefined })
  }

  openPopover = (target, step) => {
    this.setState({
      popoverOpen: true,
      popoverTarget: target,
      popoverData: step
    })
  }

  togglePopover() {
    this.setState({
      popoverOpen: !this.state.popoverOpen
    })
  }

  rowRenderer({ index, style }){
    const { samples, popoverOpen, popoverTarget } = this.props
    return (
      <div key={samples[index].id} style={style}>
        <Sample
          sample={samples[index]}
          popoverOpen={popoverOpen}
          popoverTarget={popoverTarget}
          openProjectPopup={this.openProjectPopup}
          openPopover={this.openPopover}
          onUpdateStatus={this.onUpdateStatus}
        />
        <hr />
      </div>
    )
  }


  render() {

    const { isLoading, samples } = this.props

    return (
      <div className='Samples'>
        {
          isLoading && samples.length === 0 &&
            <div className='Samples__empty text-message'>
              Loading…
            </div>
        }
        {
          !isLoading && samples.length === 0 &&
            <div className='Samples__empty text-message'>
              Adjust filters to view samples.
            </div>
        }

        { !isLoading && samples.length > 0 &&
          <AutoSizer>
            {({ height, width }) => (
              <List
                height={height}
                rowHeight={200}
                rowCount={samples.length}
                width={width}
                rowRenderer={this.rowRenderer}
              />
            )}
          </AutoSizer>
        }
        { this.renderPopover() }
        { this.renderProjectPopup() }
      </div>
    )
  }

  renderProjectPopup() {
    const { projectPopupID } = this.state
    const { samples, projects } = this.props
    const sample = samples.find(sample => sample.id === projectPopupID)
    let header = null
    let content = null

    if (sample) {
      const projectName = sample.data.project
      const sampleCompletion = getSampleCompletion(sample)
      let projectCompletion = getProjectCompletion(projectName, samples)
      const calculations = projects[sample.data.project]
      const steps = sample.data.pipeline.step

      if (!calculations) {
        this.props.setProjectCalculations(sample)
      }

      header = (
          <span>
            { sample.data.project }{' '}
            <span className='small text-message'>({ projectCompletion ? projectCompletion : sampleCompletion }% complete)</span>
          </span>
      )

      content = (
        <div className='project-popup-body'>
          {
            <table className='table table-bordered table-sm'>
              <tbody>
                <tr>
                  <th className='text-center'>Step Name</th>
                  <th className='text-center'>Average Time</th>
                </tr>
                {
                  sample.data.pipeline.step.map((step, i) => {
                    return(
                      <tr key={i}>
                        <td className='col-md-2'>{ step.name }</td>
                        <td className='tabular-numbers text-right'>{ (calculations && calculations[i]) ? msToHms(calculations[i]) : EMPTY_TIME }</td>
                      </tr>
                    )
                  })
                }
              </tbody>
            </table>
          }
          <StepStatusBar steps={steps} calculations={projects[sample.data.project]}/>
        </div>
      )
    }

    return (
      <Modal size='lg' isOpen={projectPopupID !== undefined} toggle={this.closeProjectPopup}>
        <ModalHeader cssModule={{'modal-title': 'w-100 text-center'}} toggle={this.closeProjectPopup}>
          { header }
        </ModalHeader>
        <ModalBody>
          { content }
        </ModalBody>
      </Modal>
    )
  }

  renderPopover() {
    const { popoverOpen, popoverTarget, popoverData } = this.state

    if (!popoverData)
      return undefined

    const step = popoverData.step
    const name = step.name
    const jobs = step.job

    return <Popover key={popoverTarget}
      placement='bottom'
      isOpen={popoverOpen}
      target={popoverTarget}
      toggle={this.togglePopover}
    >
      <PopoverHeader>Jobs for { name }</PopoverHeader>
      <PopoverBody>
        <div className='Popover__scrollArea'>
          <div className='list-group'>
            {
              jobs.map(job => {
                const statusClass = 'step__popover--' + job.status
                return(
                  <div key={job.id} className='list-group-item flex-column align-items-start'>
                    <h6 className='mb-1 text-bold'>{ job.name }</h6>
                    <div>Execution: { job.job_start_date || '' } - { job.job_end_date || '' }</div>
                    <div>Status: <span className={statusClass}>{ job.status }</span></div>
                  </div>
                )}
              )
            }
          </div>
        </div>
      </PopoverBody>
    </Popover>
  }
}

// eslint-disable-next-line react/no-deprecated
class Sample extends React.Component {
  static propTypes = {
    sample: prop.object.isRequired,
    popoverOpen: prop.bool.isRequired,
    popoverTarget: prop.element,
    openProjectPopup: prop.func.isRequired,
    openPopover: prop.func.isRequired,
    onUpdateStatus: prop.func.isRequired,
  }

  stepToScrollIntoView = null

  componentWillReceiveProps(newProps) {
    if (newProps.sample !== this.props.sample)
      this.stepToScrollIntoView = null
  }

  componentDidUpdate() {
    // We scroll into view the step-in-progress, but only once.
    // When step === true, it means we have already scrolled it once
    if (this.stepToScrollIntoView !== true && this.stepToScrollIntoView !== null) {
      const step = this.stepToScrollIntoView
      const scrollArea = step.parentElement
      const scrollBox = scrollArea.getBoundingClientRect()
      scrollArea.scrollLeft = step.offsetLeft - scrollBox.left - scrollBox.width / 2
      this.stepToScrollIntoView = true
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.sample === this.props.sample
      && nextProps.popoverOpen === this.props.popoverOpen
      && nextProps.popoverTarget === this.props.popoverTarget)
      return false
    return true
  }

  render() {
    const { sample } = this.props
    const submission_date = (sample.data.submission_date + '').split('T')[0]

    return (
      <div className='Sample'>
        <div className='Sample__details box'>
          <div className='box__header'>
          <div className='box__title'>
              <Span>{ sample.id }</Span>
            </div>
            <span>
              { sample.data.project &&
                  <Button className='btn-xs' onClick={() => this.props.openProjectPopup(sample.id)}>Open Project</Button>
              }
            </span>
          </div>
          <div className='box__body'>
            <div className='Sample__table'>
              <div className='d-flex'>
                <span className='th'>Pipeline</span>
                <Span>{ sample.data.pipeline.name } { sample.data.pipeline.general_information.pipeline_version }</Span>
                <span className='th'>Assembly</span>
                <Span>{ sample.data.pipeline.general_information.assembly_used } ({ sample.data.pipeline.general_information.assembly_source  + ')'}</Span>
              </div>
              <div className='d-flex'>
                <span className='th'>HPC Center</span>
                <Span>{ sample.data.pipeline.general_information.server }</Span>
                <span className='th'>DBSNP</span>
                <Span>{ sample.data.pipeline.general_information.dbsnp_version }</Span>
              </div>
              <div className='d-flex'>
                <span className='th'>Species</span>
                <Span>{ sample.data.pipeline.general_information.analysed_species }</Span>
                <span className='th'>Folder</span>
                <Span colSpan='3'>{ sample.data.pipeline.general_information.analysis_folder }</Span>
              </div>
              <div className='d-flex'>
                <span className='th'>Avg. Step Time</span>
                <Span>{ msToHms(getStepDuration(sample)[0]) }</Span>
                <span className='th'>Time Elapsed</span>
                <Span colSpan='3'>{ msToHms(getStepDuration(sample)[1]) }</Span>
              </div>
              <div className='d-flex'>
                <span className='th'>User</span>
                <Span>{ sample.user }</Span>
                <span className='th'>Completion</span>
                <Span colSpan='3'>{ getSampleCompletion(sample) }%</Span>
              </div>
              <div className='d-flex'>
                <span className='th'>Project</span>
                <Span>{ sample.data.project }</Span>
                <span className='th'>Submission Date</span>
                <Span colSpan='3'>{ sample.data.submission_date ? submission_date : '' }</Span>
              </div>
              <div className='d-flex'>
                <span className='th'>Last Modified</span>
                <Span>{ humanReadableTime(sample.modified) }</Span>
              </div>
            </div>
          </div>
        </div>
        { this.renderSteps(sample) }
      </div>
    )
  }

  renderSteps(sample) {
    if (!sample || !sample.data)
      return <em>No steps</em>

    const steps = sample.data.pipeline.step

    return (
      <div className='steps'>
        <div className='Sample__steps steps'>
            {
              steps.map((step, i) => {
                const id = ['step', sample.id, i].join('__')

                // We need to scroll to the last done/in-progress job, thus we attach a ref
                const onRef = ref => {
                  if (this.stepToScrollIntoView !== true)
                    this.stepToScrollIntoView = ref
                }

                return (
                  <Step
                    key={i}
                    sample={sample}
                    step={step}
                    index={i}
                    active={this.props.popoverOpen && id === this.props.popoverTarget}
                    onUpdateStatus={this.props.onUpdateStatus}
                    onOpenPopover={this.props.openPopover}
                    onRef={onRef}
                  />
                )
              })
            }
        </div>
        <StepStatusBar steps={steps}/>
      </div>
    )
  }
}

/**
 * getStepDuration()[0] returns average, getStepDuration[1] returns total
 * @param {Object} sample
 */
function getStepDuration(sample) {
  const steps = sample.data.pipeline.step
  let stepDuration = 0
  steps.forEach(step => {
    let jobTimes = []
    step.job.forEach(job => {
      if (job.job_start_date && job.job_end_date)
        jobTimes.push(model.timeTaken(job.job_start_date, job.job_end_date))
    })
    jobTimes.forEach(time => {
      if (time >= 0)
        stepDuration += time
    })
  })
  return [stepDuration / steps.length, stepDuration]
}

function getSampleCompletion(sample) {
  const steps = sample.data.pipeline.step
  let completedSteps = 0;
  steps.forEach(step => {
    let status = model.getStepStatus(step)
    if ((status !== STEP_STATUS.UNDETERMINED) && (status !== STEP_STATUS.QUEUED) && (status !== STEP_STATUS.RUNNING))
      completedSteps++
  })
  return parseInt((completedSteps / steps.length * 100).toFixed(0))
}


/**
 * Project completion based on projects samples completionS
 * @param {string} project
 * @param {Object[]} samples
 * @returns {number}
 */
function getProjectCompletion(project, samples) {
  if (project === undefined)
    return undefined

  const samplesForProject = samples.filter(u => u.data.project === project)
  const completions = []
  samplesForProject.forEach(sample => {
    completions.push(getSampleCompletion(sample))
  })

  let total = 0;
  completions.forEach(completion => {
    if (completion >= 0)
      total += completion
  })
  return (total / samplesForProject.length).toFixed(0)
}


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Samples);
