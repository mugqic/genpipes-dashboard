import React, { Component } from 'react';
import { compose } from 'redux'
import { connect } from 'react-redux';
import { identity, reverse } from 'ramda';
import { Alert } from 'reactstrap'

import { PROPERTIES } from '../constants.js';
import smartSort from '../helpers/smartSort.js';
import Samples from './Samples.js';
import Header from './Header'


const mapStateToProps = state => ({
    isLoading: state.samples.isLoading
  , samples: state.samples.list
  , selector: PROPERTIES[state.ui.ordering.property].selector
  , ascending: state.ui.ordering.ascending
  , errorMessage: state.ui.errorMessage
})

class App extends Component {

  render() {

    const { isLoading, samples, selector, ascending, errorMessage } = this.props

    const sortedSamples =
      (ascending ? identity : reverse)([...samples].sort((a, b) =>
        smartSort(selector(a), selector(b))))

    return (
      <div className='App__wrapper'>
        <Header />
        <div className='App'>
          <Alert isOpen={errorMessage !== undefined} color='danger'>{ errorMessage }</Alert>
          <Samples isLoading={isLoading} samples={sortedSamples} />
        </div>
      </div>
    )
  }
}


export default connect(
  mapStateToProps,
)(App);
