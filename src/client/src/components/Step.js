/*
 * Step.js
 */

import React from 'react';
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';
import cx from 'classname';
import { faExclamation } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import fireEvent from '../helpers/fire-event.js'

import { STEP_STATUS } from '../constants'
import { getStepStatus } from '../models'

export default class Step extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this)
    this.onClick = this.onClick.bind(this)
    this.onContextMenu = this.onContextMenu.bind(this)

    this.state = {
      open: false
    }
  }

  onClick(ev) {
    if (this.dropdown && this.dropdown.contains(ev.target))
      return
    const { sample, step, onOpenPopover } = this.props
    const id = this.getID()
    onOpenPopover(id, { step, sample })
  }

  onContextMenu(ev) {
    ev.preventDefault()
    fireEvent(document.body, 'click')
    this.open()
  }

  getID() {
    const { sample, index } = this.props
    return ['step', sample.id, index].join('__').replace(/\./g, '_')
  }

  updateStatus(status) {
    const { sample, step, index, onUpdateStatus } = this.props
    onUpdateStatus(sample, step, index, status)
  }

  open() {
    this.setState({ open: true })
  }

  toggle() {
    this.setState({ open: !this.state.open })
  }

  render() {
    const { step, active, onRef } = this.props

    const id = this.getID()

    const jobs = step.job
    const name = step.name
    const endDate = getLastEndDate(jobs)

    const status = getStepStatus(step)

    const isDone = jobs.every(hasEnded)
    const inProgress = status === STEP_STATUS.RUNNING
    const queued = status === STEP_STATUS.QUEUED

    const className = cx(
        'step',
      { 'step--success':  status === STEP_STATUS.SUCCESS },
      { 'step--error':    status === STEP_STATUS.ERROR },
      { 'step--in-progress': inProgress },
      { 'step--queued': queued },
      { 'step--active': active || this.state.open }
    )

    return (
      <div
        id={id}
        className={className}
        onClick={this.onClick}
        onContextMenu={this.onContextMenu}
        ref={!(inProgress || isDone) ? undefined : onRef}
      >
        <div className='step__icon'>
          { status === STEP_STATUS.ERROR && <FontAwesomeIcon icon={faExclamation} /> }
        </div>
        <div className='step__wrapper'>
          <div className='step__dot' />
          <div className='step__name' title={name}>{name}</div>
        </div>
        <div className='step__dropdown' ref={e => e && (this.dropdown = e)}>
          <Dropdown direction='right' isOpen={this.state.open} toggle={this.toggle}>
            <DropdownToggle
              tag='span'
              data-toggle="dropdown"
              aria-expanded={this.state.open}
            />
            <DropdownMenu>
              <DropdownItem header>Change Status</DropdownItem>
              <DropdownItem onClick={() => this.updateStatus('success')}>Success</DropdownItem>
              <DropdownItem onClick={() => this.updateStatus('error')}>Error</DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </div>
      </div>
    )
  }
}



function hasEnded(job) {
  return 'job_end_date' in job
}

function getLastEndDate(jobs) {
  const result = jobs.reduce((acc, job) => {
    if (!job.job_end_date)
      return acc
    const d = new Date(job.job_end_date)
    if (d > acc)
      return d
    return acc
  }, 0)
  return result === 0 ? undefined : result.toLocaleString()
}
