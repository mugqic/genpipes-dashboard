/*
 * Icon.js
 */

import React from 'react';

export default function Icon({ name, className = '', spin }) {
  let iconClassName = `fa fa-${name}`

  if (className)
    iconClassName += ' ' + className

  if (spin)
    iconClassName += ' fa-spin'

  return (
    <i className={iconClassName} />
  )
}
