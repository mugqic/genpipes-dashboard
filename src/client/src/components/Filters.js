/*
 * Filters.js
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Button,
  ButtonGroup,
  UncontrolledButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Popover,
  PopoverBody,
  PopoverHeader,
  Input,
  InputGroup,
  InputGroupAddon
} from 'reactstrap';
import { compose } from 'ramda';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import Icon from './Icon'
import { fetchSamples, setParamAndSearch, clearParamsAndSearch } from '../actions.js';
import * as Requests from '../requests.js'
import { SAMPLE_STATUS, STEP_STATUS_OPTIONS } from '../constants'


const mapStateToProps = state => ({
  samples: state.samples.list,
  searchParams: state.ui.searchParams,
  params: state.ui.params,
  paramsLoaded: state.ui.paramsLoaded,
})
const mapDispatchToProps = dispatch => ({
  clearParamsAndSearch: compose(dispatch, clearParamsAndSearch),
  fetchSamples: compose(dispatch, fetchSamples),
  setParamAndSearch: compose(dispatch, setParamAndSearch),
})

class Filters extends Component {

  constructor(props){
    super(props)
    this.state = {
      helpPopoverOpen: false,
      searchInput: ''
    }
    this.toggleHelp = this.toggleHelp.bind(this)
    this.clickDropdown = this.clickDropdown.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.clearInput = this.clearInput.bind(this)
  }

  toggleHelp(){
    this.setState({ helpPopoverOpen: !this.state.helpPopoverOpen})
  }

  clickDropdown(){
    this.setState({ searchInput: '' })
  }

  handleChange(e){
    this.setState({ searchInput: e.target.value })
  }

  clearInput(id) {
    this.setState({ searchInput: '' })
    document.getElementById(id).value = ''
  }

  render() {

    // All search params displayed in dropdown menu
    const { clearParamsAndSearch, setParamAndSearch, searchParams, params, paramsLoaded } = this.props
    const { searchInput } = this.state

    return (
      <div className='d-flex'>
        <div className='flex-static'>
          <span className='label'>Pipeline</span>
          <UncontrolledButtonDropdown className='dropdown--inline' onClick={this.clickDropdown}>
            <DropdownToggle caret>
              { params.pipeline || (paramsLoaded ? <span className='text-muted'>All</span> : <Icon name='spinner' spin />) }
            </DropdownToggle>
            <DropdownMenu>
              <div className='flex'>
              <InputGroup>
                <Input id='pipelineInput' placeholder='Filter' className='dropdown--input' onChange={this.handleChange}/>
                <InputGroupAddon className='input-group-addon'>
                  <Button className='button-invisible'>
                    <Icon name='remove' />
                  </Button>
                </InputGroupAddon>
              </InputGroup>
              </div>
              <DropdownItem className='text-message' onClick={() => setParamAndSearch('pipeline', undefined)}>All</DropdownItem>
              {
                searchParams.pipelines.map(value => {
                  return (
                    value.startsWith(searchInput.charAt(0).toUpperCase() + searchInput.slice(1)) &&
                      <DropdownItem key={ value }
                        onClick={() => setParamAndSearch('pipeline', value)}>{ value }</DropdownItem>
                  )
                })
              }

            </DropdownMenu>
          </UncontrolledButtonDropdown>
        </div>

        <span className='flex-static vertical-hr' />
        <div className='flex-static'>
          <span className='label'>User</span>
          <UncontrolledButtonDropdown className='dropdown--inline' onClick={this.clickDropdown}>
            <DropdownToggle caret>
              { params.user || (paramsLoaded ? <span className='text-muted'>All</span> : <Icon name='spinner' spin />) }
            </DropdownToggle>
            <DropdownMenu>
              <div className='flex'>
              <InputGroup>
                <Input id='userInput' placeholder='Filter' className='dropdown--input' onChange={this.handleChange}/>
                <InputGroupAddon className='input-group-addon'>
                  <Button className='button-invisible' onClick={() => this.clearInput('userInput')}>
                    <Icon name='remove' />
                  </Button>
                </InputGroupAddon>
              </InputGroup>
              </div>
              <DropdownItem className='text-message' onClick={() => setParamAndSearch('user', undefined)}>All</DropdownItem>
              {
                searchParams.users.map(value => {
                  return (
                    value.startsWith(searchInput.charAt(0) + searchInput.slice(1)) &&
                      <DropdownItem key={ value }
                        onClick={() => setParamAndSearch('user', value)}>{ value }</DropdownItem>
                  )
                })
              }

            </DropdownMenu>
          </UncontrolledButtonDropdown>
        </div>

        <span className='flex-static vertical-hr' />
        <div className='flex-static'>
          <span className='label'>Status</span>
          <UncontrolledButtonDropdown className='dropdown--inline'>
            <DropdownToggle caret>
              { params.status ? STEP_STATUS_OPTIONS.find(p => p.value === params.status).text : (paramsLoaded ? <span className='text-muted'>All</span> : <Icon name='spinner' spin />) }
            </DropdownToggle>
            <DropdownMenu>
              <DropdownItem className='text-message' onClick={() => setParamAndSearch('status', undefined)}>All</DropdownItem>
              {
                STEP_STATUS_OPTIONS.map(option => {
                  return (
                    <DropdownItem key={option.value}
                      onClick={() => setParamAndSearch('status', option.value)}>{ option.text }</DropdownItem>
                  )
                })
              }
            </DropdownMenu>
          </UncontrolledButtonDropdown>
        </div>

        <span className='flex-static vertical-hr' />
        <div className='flex-static'>
          <span className='label'>Project</span>
          <UncontrolledButtonDropdown className='dropdown--inline' onClick={this.clickDropdown}>
            <DropdownToggle caret>
              { titleCase(params.project) || (paramsLoaded ? <span className='text-muted'>All</span> : <Icon name='spinner' spin />) }
            </DropdownToggle>
            <DropdownMenu>
              <div className='flex'>
              <InputGroup>
                <Input id='projectInput' placeholder='Filter' className='dropdown--input' onChange={this.handleChange}/>
                <InputGroupAddon className='input-group-addon'>
                  <Button className='button-invisible' onClick={() => this.clearInput('projectInput')}>
                    <Icon name='remove' />
                  </Button>
                </InputGroupAddon>
              </InputGroup>

              </div>
              <DropdownItem className='text-message' onClick={() => setParamAndSearch('project', undefined)}>All</DropdownItem>
              {
                searchParams.projects.map(value => {
                  return (
                    value !== null && value.startsWith(searchInput.charAt(0) + searchInput.slice(1)) &&
                      <DropdownItem key={ value }
                        onClick={() => setParamAndSearch('project', value)}>{ value }</DropdownItem>
                  )
                })
              }
            </DropdownMenu>
          </UncontrolledButtonDropdown>
        </div>

        <div className='flex-fill' />
        <span className='flex-static vertical-hr' />
        <button className='App__helpButton' onClick={this.toggleHelp}><FontAwesomeIcon className='App__helpIcon' id='helpIcon' icon={faQuestionCircle} /></button>
        <div className='flex-static'>
          <Button onClick={clearParamsAndSearch}>
            <Icon name='remove' /> <span className='sr-only'>Clear Filters</span>
          </Button>
        </div>
        <Popover className='App__helpPopover' placement='left' isOpen={this.state.helpPopoverOpen} toggle={this.toggleHelp} target='helpIcon'>
          <PopoverHeader className='text-center'>How to Search for Samples</PopoverHeader>
          <PopoverBody>
            <p>
              Select a date range for the samples you are looking for.
              Add additional filters for pipeline, user, status, and project
              to narrow your results. On the far left, you can specify how the
              samples are to be ordered, as well as whether the values are
              ascending or descending.
            </p>
          </PopoverBody>
        </Popover>
      </div>
    )
  }
}

function titleCase(value) {
  if (!value)
    return ''
  return value.charAt(0).toUpperCase() + value.slice(1)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Filters);
