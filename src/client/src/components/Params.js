/*
 * Params.js
 */


import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  Button,
  Popover,
} from 'reactstrap';
import { format } from 'date-fns';
import { DateRangePicker } from 'react-date-range';
import { endOfDay } from 'date-fns';

import Icon from './Icon'
import { setParam, setModifiedRange, fetchSamples } from '../actions.js';



const mapStateToProps = state => ({
  isLoading: state.samples.isLoading,
  params: state.ui.params,
})
const mapDispatchToProps = dispatch =>
  bindActionCreators({ setParam, setModifiedRange, fetchSamples }, dispatch)

class Params extends Component {
  constructor(props) {
    super(props)
    this.state = {
      popoverOpen: false,
    }
  }

  togglePopover = () => {
    this.setState({ popoverOpen: !this.state.popoverOpen })
  }

  onChangeModifiedRange = ranges => {
    if (ranges.modified.startDate === ranges.modified.endDate) {
      if (!this.isModified) {
        this.isModified = true
        this.props.setParam('modifiedAfter', ranges.modified.startDate)
        this.props.setParam('modifiedBefore', endOfDay(ranges.modified.endDate))
        return
      }
    }
    this.isModified = false
    this.props.setModifiedRange(ranges.modified.startDate, endOfDay(ranges.modified.endDate))
    this.props.fetchSamples()
    this.setState({ popoverOpen: false })
  }

  render() {
    const { isLoading, params } = this.props

    const modifiedRange = {
      key: 'modified',
      startDate: params.modifiedAfter,
      endDate:   params.modifiedBefore,
    }

    return (
      <div className='d-inline-block'>
        <Button id='calendar-button' onClick={this.togglePopover} disabled={isLoading}>
          <Icon name='calendar' />{' '}
          { format(params.modifiedAfter, 'MMM D, YYYY') } - { format(params.modifiedBefore, 'MMM D, YYYY') } {
            isLoading &&
              <Icon name='spinner' spin />
          }
        </Button>
        <Popover placement='bottom' isOpen={this.state.popoverOpen} target='calendar-button' toggle={this.togglePopover}>
          <DateRangePicker
            months={2}
            direction='horizontal'
            ranges={[modifiedRange]}
            onChange={this.onChangeModifiedRange}
          />
        </Popover>
      </div>
    )
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Params);
