import React from 'react'
import { Navbar } from 'reactstrap';

import Filters from './Filters'
import Order from './Order'
import Params from './Params'
import genpipesLogo from '../logos/genpipesLogo.png'

class Header extends React.Component {

  render(){
    return (
      <div className='header'>
          <Navbar className='header__navbar' light expand="md">
            <img className='header__logo' src={genpipesLogo} alt='GenPipes' />
          </Navbar>
          <div className='App__row button__padding'>
            <div className='group'>
              <Order />
              <div className='vertical-hr d-inline-block'/>
              <Params />
            </div>
            <div className='group'>
              <Filters />
            </div>
          </div>
      </div>
    )
  }
}

export default Header