import React from 'react';
import msToHms from 'ms-to-hms'
import { Tooltip } from 'react-tippy'

import { getStepStatus, timeTaken } from '../models'
import { STEP_STATUS, STEP_STATUS_OPTIONS } from '../constants'

const classByStatus = {
  [STEP_STATUS.SUCCESS]:      'StepStatusBar__step--success',
  [STEP_STATUS.ERROR]:        'StepStatusBar__step--error',
  [STEP_STATUS.UNDETERMINED]: 'StepStatusBar__step--undetermined',
  [STEP_STATUS.SUCCESS]:      'StepStatusBar__step--success',
  [STEP_STATUS.QUEUED]:  'StepStatusBar__step--queued',
  [STEP_STATUS.RUNNING]: 'StepStatusBar__step--running'
}

let nextId = 1
export default class StepStatusBar extends React.Component{
  id = (nextId++).toString()

  render(){
    const { steps, calculations } = this.props

    let totalStepTime = 0
    if (!calculations) {
      steps.forEach(step => {
        let jobTime = 0
        step.job.forEach(job => {
          if (job.job_start_date && job.job_end_date){
            const timeElapsed = timeTaken(job.job_start_date, job.job_end_date)
            if(!Number.isNaN(timeElapsed))
              jobTime += timeElapsed
          }
        })
        totalStepTime += jobTime
        step.time = jobTime
      })
    }
    else {
      for (let i = 0 ; i < steps.length ; i++) {
        if ( !Number.isNaN(calculations[i])) {
          totalStepTime += calculations[i]
          steps[i].time = calculations[i]
        }
        else
          steps[i].time = 0
      }
    }


    return(
      <div className='StepStatusBarWrapper flex'>
        {
          steps.map((step, i) => {
            let timePercentage = step.time / totalStepTime * 100
            if (timePercentage < 1)
              timePercentage = .1 / steps.length * 100

            const stepStatus = getStepStatus(step)
            const klass = classByStatus[stepStatus]
            return (
              <Tooltip
                className={['StepStatusBar', klass].join(' ')}
                style={{'--stepWidth': timePercentage + '%'}}
                key={i}
                duration='0'
                arrow
                followCursor
                html={(
                  <div className='step__tooltip'>
                    <p className='step__tooltip__text'>{ step.name + ': ' + STEP_STATUS_OPTIONS.find(o => o.value === stepStatus).text }</p>
                    <p className='step__tooltip__text'>{ msToHms(step.time) }</p>
                  </div>
                )}>
              </Tooltip>
            )
          })
        }
      </div>
    )
  }
}
