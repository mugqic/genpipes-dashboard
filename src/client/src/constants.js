/*
 * constants.js
 */

const { view, lensPath, converge } = require('ramda');

const getSampleStatus = require('./models')

// Values start with a number for sorting
const SAMPLE_STATUS = {
  RUNNING:      '0 - RUNNING',
  ERROR:        '1 - ERROR',
  UNDETERMINED: '3 - UNDETERMINED',
  SUCCESS:      '5 - SUCCESS',
}

const STEP_STATUS = {
  RUNNING:      '0 - RUNNING',
  ERROR:        '1 - ERROR',
  UNDETERMINED: '3 - UNDETERMINED',
  SUCCESS:      '5 - SUCCESS',
  QUEUED:       '6 - QUEUED'
}

const PROPERTIES = {
  lastUpdate: { name: 'Last Update', selector: view(lensPath(['modified'])) },
  id:         { name: 'ID',          selector: view(lensPath(['id'])) },
  user:       { name: 'User',        selector: view(lensPath(['user'])) },
  pipeline:   { name: 'Pipeline',    selector:
    converge((name, version) => [name, version].join(' '), [
      view(lensPath(['data', 'pipeline', 'name'])),
      view(lensPath(['data', 'pipeline', 'general_information', 'pipeline_version']))]) },
  project:    { name: 'Project',     selector: view(lensPath(['project'])) },
  status:     {
    name: 'Status',
    selector: getSampleStatus,
    options: toOptions(SAMPLE_STATUS)
  }
}

const STEP_STATUS_OPTIONS = [
  { text: 'Running',      value: STEP_STATUS.RUNNING },
  { text: 'Error',        value: STEP_STATUS.ERROR },
  { text: 'Undetermined', value: STEP_STATUS.UNDETERMINED },
  { text: 'Success',      value: STEP_STATUS.SUCCESS },
  { text: 'Queued',       value: STEP_STATUS.QUEUED },
]


function toOptions(object) {
  const values = []
  Object.keys(object).forEach(key => {
    values.push({ text: key.toLowerCase(), value: object[key] })
  })
  return values
}

module.exports = { SAMPLE_STATUS, STEP_STATUS, PROPERTIES, STEP_STATUS_OPTIONS, toOptions }
