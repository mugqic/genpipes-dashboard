import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';

import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.min.css';
import 'react-date-range/dist/styles.css';
import 'react-date-range/dist/theme/default.css';
import 'react-tippy/dist/tippy.css';
import 'react-virtualized/styles.css';

import './styles.css';
import registerServiceWorker from './registerServiceWorker';
import { rootReducer } from './reducers';
import App from './components/App';
import { initializeParams, setErrorMessage, fetchSamples } from './actions.js';
import * as Requests from './requests.js'


const initialState = {}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store =
  (process.env.NODE_ENV === 'production')
  ? createStore(rootReducer, initialState, applyMiddleware(thunkMiddleware))
  : createStore(rootReducer, initialState, composeEnhancers(applyMiddleware(thunkMiddleware, createLogger())))

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.querySelector('#root')
)


// Load initial data

Requests.samples.searchParams()
.then(res => {
  store.dispatch(initializeParams(res))
})
.catch(err => {
  store.dispatch(setErrorMessage('Error Occured: ' + err.message))
})

store.dispatch(fetchSamples())


// Register service worker

registerServiceWorker()

// HMR

if (module.hot) {
  module.hot.accept(['./components/App'], () => {
    const NextApp = require('./components/App').default;
    render(
      <Provider store={store}>
        <NextApp />
      </Provider>,
      document.querySelector('#root')
    );
  });
}
