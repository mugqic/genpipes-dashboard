/*
 * requests.js
 */

import isObject from './helpers/is-object'
import queryString from './helpers/query-string'

export function fetchJSON(url, data, options = {}) {
  const fetchOptions = {
    credentials: 'same-origin',
    headers: {},
    ...options
  }
  if (isObject(data)) {
    if (fetchOptions.method === 'post') {
      fetchOptions.body = JSON.stringify(data)
      fetchOptions.headers['content-type'] = 'application/json'
    } else {
      url += `?${queryString(data)}`
    }
  }

  return fetch(url, fetchOptions)
    .then(res => res.json())
    .then(res => res.ok ? Promise.resolve(res.data) : Promise.reject(res))
}

export function fetchAPI(url, data, options) {
  return fetchJSON(process.env.PUBLIC_URL + '/api' + url, data, options)
}


// API calls

export const samples = {
  list:   (params)   => get('/samples', params),
  update: (id, data) => post(`/samples/update/${id}`, data),
  searchParams: () => get('/samples/get-search-params'),
  search: (params) => get('/samples/search', params)
}


// Helpers

function post(url, data, options) {
  return fetchAPI(url, data, { method: 'post' })
}

function get(url, data) {
  return fetchAPI(url, data, { method: 'get' })
}
