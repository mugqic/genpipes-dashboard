const express = require('express')
const path = require('path')
const logger = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const helmet = require('helmet')
const csp = require('express-csp-header')
const compression = require('compression')

const db = require('./database.js')
const Samples = require('./models/samples.js')


const app = express()


/*
 * View engine
 */

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jade')


/*
 * Logger & middlewares
 */

app.use(logger('dev'))
app.use(helmet())
app.use(compression())
app.use(csp({ policies: { 'default-src': [csp.SELF], 'img-src': [csp.SELF, 'data:'], 'style-src': [csp.SELF, csp.INLINE] } }))
app.use(bodyParser.json({ limit: '1000mb' }))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))


/*
 * Routes
 */

app.use('/api/samples', require('./routes/samples'))

app.use('/', (req, res, next) => {
  Samples.getAll()
  .then(samples => {
    res.render('index', { title: 'Analysis Portal', samples: samples })
  })
})


/*
 * Finally, 404 and error handler
 */

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
