const express = require('express')
const router = express.Router()

const Samples = require('../models/samples.js')
const { okHandler, dataHandler, errorHandler } = require('../helpers/handlers.js')
const applyDiff = require('../helpers/apply-diff')
const { getSampleStatus } = require('../client/src/models')


router.get('/get-search-params', (req,res,next) =>{
  Samples.getSearchParams()
  .then(dataHandler(res))
  .catch(errorHandler(res))
})

router.get('/search', (req,res,next) => {
  Samples.search(req.query)
  .then(dataHandler(res))
  .catch(errorHandler(res))
})

router.get('/:id', (req, res, next) => {
  const id = req.params.id
  Samples.get(id)
  .then(dataHandler(res))
  .catch(errorHandler(res))
})

router.post('/update/:id', (req, res, next) => {
  const id = req.params.id
  const data = req.body

  Samples.update(id, data)
  .then(okHandler(res))
  .catch(errorHandler(res))
})

router.post('/external-update/:user', (req, res, next) => {
  const data      = req.body
  const sample    = {}
  sample.data     = data
  const id        = data.sample_name
  const user      = req.params.user
  const status    = getSampleStatus(sample)
  const pipeline  = [data.pipeline.name, data.pipeline.general_information.pipeline_version].join(' ')
  const project   = data.project || 'undefined'

  Samples.insert(id, user, data, status, pipeline, project)
  .then(okHandler(res))
  .catch(errorHandler(res))
})

router.post('/external-update-diff/:user', (req, res, next) => {
  const user       = req.params.user
  const operations = req.body.operations
  const id         = req.body.sample_name

  Samples.get(id)
  .then(row => {
    const data = applyDiff(row.data, operations)
    const status     = getSampleStatus({ data })
    const pipeline   = [data.pipeline.name, data.pipeline.general_information.pipeline_version].join(' ')
    const project    = data.project || 'undefined'
    return Samples.insert(id, user, data, status, pipeline, project)
  })
  .then(okHandler(res))
  .catch(errorHandler(res))
})

module.exports = router;
