/*
 * samples.js
 */


const { query, selectOne, selectAll } = require('../database')
const { getSampleStatus } = require('../client/src/models')

module.exports = {
  insert,
  get,
  update,
  getSearchParams,
  search,
}

function insert(id, user, data, status, pipeline, project) {
  return query(`
    INSERT INTO samples (id, "user", data, created, modified, "status", pipeline, project)
         VALUES         (@id, @user, @data, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, @status, @pipeline, @project)
    ON CONFLICT (id) DO
         UPDATE
            SET data = @data
              , "user" = @user
              , modified = CURRENT_TIMESTAMP
              , status = @status
              , pipeline = @pipeline
              , project = @project
          WHERE samples.id = @id;
  `, { id, user, data, status, pipeline, project })
}

function update(id, data) {
  return query(`
    UPDATE samples
       SET data = @data
         , modified = CURRENT_TIMESTAMP
    WHERE id = @id;
  `, { id, data })
}

function get(id) {
  return selectOne(`
    SELECT * FROM samples WHERE id = @id
  `, { id })
}


function getSearchParams(){
  return Promise.all([
    selectAll('SELECT DISTINCT "user" FROM samples').then(rows => rows.map(r => r.user)),
    selectAll('SELECT DISTINCT pipeline FROM samples').then(rows => rows.map(r => r.pipeline)),
    selectAll('SELECT DISTINCT project FROM samples').then(rows => rows.map(r => r.project))
  ])
  .then(([users, pipelines, projects]) => ({ users, pipelines, projects }));
}

function search(params){
  const conditions = ['1 = 1']

  if (params.modifiedAfter)
    conditions.push('modified > @modifiedAfter')

  if (params.modifiedBefore)
    conditions.push('modified < @modifiedBefore')

  if (params.createdAfter)
    conditions.push('created > @createdAfter')

  if (params.createdBefore)
    conditions.push('created < @createdBefore')

  const q = `
    SELECT *
      FROM samples
     WHERE ${conditions.join(' AND ')}
  ORDER BY modified DESC
  `

  return selectAll(q, params)
  .then(rows => {

    if (params.user !== undefined)
      rows = rows.filter(u => u.user === params.user)

    if (params.status !== undefined)
      rows = rows.filter(u => getSampleStatus(u) === params.status)

    if (params.project !== undefined)
      rows = rows.filter(u => u.data.project === params.project)

    if (params.pipeline !== undefined)
      rows = rows.filter(u => (u.data.pipeline.name.concat(' ' + u.data.pipeline.general_information.pipeline_version)) === params.pipeline)

    return rows
  })
}

// Helpers

