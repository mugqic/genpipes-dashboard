/*
 * apply-diff.js
 */

const { set, over, lensPath, dissocPath, insert, remove } = require('ramda')


module.exports = applyDiff


function applyDiff(root, operations) {
  let result = root

  operations.forEach(({ op, path, index, value }) => {
    switch (op) {
      case 'add': result = set(lensPath(path), value, result); break;
      case 'replace': result = set(lensPath(path), value, result); break;
      case 'delete': result = dissocPath(path, result); break;
      case 'insert': result = over(lensPath(path), list => insert(index, value, list), result); break;
      case 'remove': result = over(lensPath(path), list => remove(index, 1, list), result); break;
    }
  })

  return result
}
