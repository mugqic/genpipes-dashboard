#!/usr/bin/env node
/*
 * call-remote-scripts.js
 */

const fs = require('fs')
const path = require('path')
const util = require('util')
const childProcess = require('child_process')
const nodemailer = require('nodemailer')
const config = require('./config')

const exec = util.promisify(childProcess.exec)
const readFile = util.promisify(fs.readFile)
const writeFile = util.promisify(fs.writeFile)

const ssh =
    'ssh -o BatchMode=yes'
  + ' -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null'
  + ' -i /home/genpipes-dashboard-fetcher/.ssh/id_ed25519'

const ssh_nobatch =
    'ssh'
  + ' -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null'
  + ' -i /home/genpipes-dashboard-fetcher/.ssh/id_ed25519'


/* This is run for the c3g.ca new website, so it has access to the versions of things installed
 * on beluga. We do it here because we already have an automated & programatic access to beluga,
 * which is a bit hard to set up. */
const cvmfsCommand = ` 
  /cvmfs/soft.mugqic/CentOS6/software/python/Python-3.7.3/bin/python
    /project/6007512/C3G/analyste_dev/software/genpipes/genpipes_test/resources/modules/module_helpers/json_combine.py
    --path /genfs/projects/analyste_dev/mugqic_home/CentOS6/modulefiles/mugqic
        > ${path.join(__dirname, './public/cvmfs-modules.json')}
`.replace(/\n/g, '').trim()

const commands = [
  { name: 'cedar',  value: `${ssh_nobatch} genpipes-portal@cedar.computecanada.ca   /project/6007512/C3G/analyste_dev/genpipes-portal-send` },
  { name: 'graham', value: `${ssh_nobatch} genpipes-portal@graham.computecanada.ca  /project/6002326/C3G/analyste_dev/genpipes-portal-send` },
  { name: 'beluga', value: `${ssh_nobatch} genpipes-portal@beluga.calculcanada.ca   /project/6007512/C3G/analyste_dev/genpipes-portal-send` },
  { name: 'narval', value: `${ssh_nobatch} genpipes-portal@narval.calculcanada.ca   /project/6007512/C3G/analyste_dev/genpipes-portal-send` },
  { name: 'cvmfs',  value: `${ssh_nobatch} genpipes-portal@beluga.calculcanada.ca   ${cvmfsCommand}` },
]

const MAX_RETRIES = 10
const NOTIFICATION_INTERVAL = 24 * 60 * 60 * 1000 /* 1 email/day max */

const EMPTY_ERRORS = {
  cedar:  0,
  graham: 0,
  beluga: 0,
  narval: 0,
  cvmfs: 0,
}

const EMPTY_STATE = {
  lastNotification: null,
  errors: EMPTY_ERRORS
}

const stateFilename = '/usr/local/share/genpipes-dashboard/state.json'

const transporter = nodemailer.createTransport(config.nodemailer)
const sendEmail = options =>
  transporter.sendMail({
    from: config.email.from,
    to: config.email.to,
    ...options
  })


let state = undefined

run().catch(e => { console.error(e) })



async function run() {
  const results = await Promise.all(
    commands.map(c => {
      console.log(`[${c.name}]: ${c.value}`)
      return exec(c.value, {maxBuffer: 5096 * 5096})
        .then(result => ({ ok: true, ...result }))
        .catch(error => ({ ok: false, error, command: c }))
    }))

  console.log(results)

  try {
    state = await readState()

    if (results.some(r => !r.ok))
      await sendNotification(results.filter(r => !r.ok))
    else
      state.errors = EMPTY_ERRORS

    await writeState()

  } catch (err) {
    await sendEmail({
      subject: 'Genpipes-dashboard: script error',
      html: `
        Hello,<br/>
        <br/>
        An error occured while running the fetch script:<br/>
        <pre>${err.toString()}</pre>
        <br/>
      `
    })
  }
}

async function sendNotification(results) {
  let maxErrorCount = 0

  results.forEach(r => {
    state.errors[r.command.name] += 1

    if (state.errors[r.command.name] > maxErrorCount)
      maxErrorCount = state.errors[r.command.name]
  })

  if (maxErrorCount < MAX_RETRIES) {
    console.log('Skipping notification: MAX_RETRIES not reached')
    return
  }

  if (state.lastNotification) {
    const diff = new Date() - new Date(state.lastNotification)

    if (diff < NOTIFICATION_INTERVAL) {
      console.log('Skipping notification: NOTIFICATION_INTERVAL not reached')
      return
    }
  }

  const report = {
    date: new Date().toISOString(),
    state,
    results
  }
  const info = await sendEmail({
    subject: 'Genpipes-dashboard: error report',
    attachments: [
      { filename: 'report.json.txt', content: JSON.stringify(report, null, 2) },
    ],
    html: `
      Hello,<br/>
      <br/>
      An error occured while fetching data.<br/>
      <br/>
    `
  })

  console.log(info)

  state.lastNotification = new Date().toISOString()
}

function readState() {
  return readFile(stateFilename)
  .then(buffer => buffer.toString())
  .then(content => JSON.parse(content))
  .catch(() => Promise.resolve(EMPTY_STATE))
}

function writeState() {
  return writeFile(stateFilename, JSON.stringify(state))
}

