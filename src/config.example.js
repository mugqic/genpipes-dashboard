/*
 * config.example.js
 */


const config = {
  email: {
    from: 'info-micm.genome@mcgill.ca',
    to: ['romain.gregoire@mcgill.ca'],
  },
  nodemailer: {
    host: 'smtp-mail.outlook.com', // hostname
    secureConnection: false, // TLS requires secureConnection to be false
    port: 587, // port for secure SMTP
    tls: {
      ciphers:'SSLv3'
    },
    auth: {
      user: 'user@example.com',
      pass: 'secret'
    }
  }
}


module.exports = config
