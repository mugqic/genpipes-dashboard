/*
 * migration-add-properties.js
 */

const db = require('../database.js')
const { getSampleStatus } = require('../client/src/models.js')


db.selectAll('SELECT * FROM samples')
.then(samples => {

  return Promise.all(samples.map(sample =>
    db.query(`
      UPDATE samples
         SET pipeline = @pipeline
           , status = @status
           , project = @project
       WHERE id = @id
    `, {
      id: sample.id,
      status: getSampleStatus(sample),
      pipeline: [sample.data.pipeline.name, sample.data.pipeline.general_information.pipeline_version].join(' '),
      project: sample.data.project || null,
    })
  ))
})
.then(() => {

  return db.selectAll('SELECT * FROM samples')
  .then(samples => {
    samples.forEach(sample => {
      console.log({ ...sample, data: null })
    })
  })
})
