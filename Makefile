#
# Makefile
# romgrk, 2017-10-24 14:33
#

all:
	make build

build:
	@printf "%b==> %bBuilding application...\n%b" '\033[1m' '\033[93m' '\033[0m'
	mkdir -p data
	@printf "%b==> %bBuilding postgres db...\n%b" '\033[1m' '\033[93m' '\033[0m'
	cd postgres && make build
	@printf "%b==> %bBuilding nodejs app...\n%b" '\033[1m' '\033[93m' '\033[0m'
	cd src && make build
	@printf "%b==> %bApplication built succesfully\n%b" '\033[1m' '\033[32m' '\033[0m'
	@printf "%b==> %bStart with %bmake run\n%b"         '\033[1m' '\033[32m' '\033[39m' '\033[0m'

build--dev:
	@printf "%b==> %bBuilding application... [DEV]\n%b" '\033[1m' '\033[93m' '\033[0m'
	mkdir -p data
	@printf "%b==> %bBuilding postgres db...\n%b" '\033[1m' '\033[93m' '\033[0m'
	cd postgres && make build
	@printf "%b==> %bBuilding nodejs app... [DEV]\n%b" '\033[1m' '\033[93m' '\033[0m'
	cd src && make build--dev
	@printf "%b==> %bApplication built succesfully\n%b" '\033[1m' '\033[32m' '\033[0m'
	@printf "%b==> %bStart with %bmake run--dev\n%b"         '\033[1m' '\033[32m' '\033[39m' '\033[0m'

run:
	make run/db &
	@printf "%b==> %bWaiting for database to be ready (5 sec)\n%b" '\033[1m' '\033[93m' '\033[0m'
	# Wait for the database to be ready
	sleep 5
	make run/app

run--dev:
	make run/db &
	@printf "%b==> %bWaiting for database to be ready (5 sec)\n%b" '\033[1m' '\033[93m' '\033[0m'
	# Wait for the database to be ready
	sleep 5
	make run/app--dev

run/db:
	cd postgres && make run

run/app:
	cd src && make run

run/app--dev:
	cd src && make run--dev

stop:
	@printf "%b==> %bStopping application...\n%b" '\033[1m' '\033[93m' '\033[0m'
	cd src && make stop
	cd postgres && make stop

clean:
	make stop
	@printf "%b==> %bCleaning...\n%b" '\033[1m' '\033[91m' '\033[0m'
	sudo rm -rf data
	mkdir -p data

.PHONY: all

